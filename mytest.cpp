#include <iostream>
#include "node.hpp"
#include "list.hpp"
#include "cat.hpp"

using namespace std;

int main(){
   
   Cat::initNames();
   
   cout<<"Hello World!" <<endl;
//   DoubleLinkedList list = DoubleLinkedList();
/*
   cout<<"Empty list created"<<endl;
   cout<<"empty "<<boolalpha<<list.empty()<<endl;
   cout<<"head: "<<list.get_first()<<endl;
   cout<<"tail: "<<list.get_last()<<endl;
   cout<<"size nil "<<list.size()<<endl; 
   
   list.push_front(Cat::makeCat());
   cout<<"###########push front"<<endl;
   cout<<"not empty "<<boolalpha<<list.empty()<<endl;
   cout<<"size 1: "<<list.size()<<endl;
   cout<<"head: "<<list.get_first()<<endl;
   cout<<"tail: "<<list.get_last()<<endl;
   
   list.push_back(Cat::makeCat());
   cout<<"###################push back"<<endl;
   cout<<"not empty "<<boolalpha<<list.empty()<<endl;
   cout<<"size 2: "<<list.size()<<endl;
   cout<<"head: "<<list.get_first()<<endl;
   cout<<"after head: "<<list.get_next(list.get_first())<<endl;
   cout<<"tail: "<<list.get_last()<<endl;
   cout<<"before tail: "<<list.get_prev(list.get_last())<<endl;

   
   //cout<<"###################popped back "<<list.pop_back()<<endl;
   cout<<"head: "<<list.get_first()<<endl;
   cout<<"after head: "<<list.get_next(list.get_first())<<endl;
   cout<<"tail: "<<list.get_last()<<endl;
   cout<<"before tail: "<<list.get_prev(list.get_last())<<endl;

   cout<<"###########push front and push back"<<endl;
   list.push_front(Cat::makeCat());
   list.push_back(Cat::makeCat());

   //cout<<"###################popped front "<<list.pop_front()<<endl;
   cout<<"head: "<<list.get_first()<<endl;
   cout<<"after head: "<<list.get_next(list.get_first())<<endl;
   cout<<"tail: "<<list.get_last()<<endl;
   cout<<"before tail: "<<list.get_prev(list.get_last())<<endl;
   cout<<"size ? "<<list.size()<<endl; 
   cout<<"new test for insert after on new list"<<endl;
   DoubleLinkedList list2;// = DoubleLinkedList();
   
   cout<<"empty true : " << list2.empty() << endl;
   cout<<"head empty "<<list2.get_first()<<endl;
   cout<<"tail empty "<<list2.get_last()<<endl;
   
   cout<<"################ pushed front "<<endl;
   list2.push_front(Cat::makeCat());
   cout<<"empty false : " << list2.empty() << endl;
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 1: "<< list2.size()<<endl;
   
   cout<<"###########insert after head"<<endl;
   list2.insert_after(list2.get_first(),Cat::makeCat());
   cout<<"empty false : " << list2.empty() << endl;
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 2: "<< list2.size()<<endl;
   
   cout<<"##########insert after tail"<<endl;
   list2.insert_after(list2.get_last(),Cat::makeCat());
   cout<<"empty false : " << list2.empty() << endl;
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 3: "<< list2.size()<<endl;
   cout<<"after head "<<list2.get_next(list2.get_first())<<endl;
   cout<<"before tail "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"new test for both insert "<<endl;
   DoubleLinkedList list3 ;

   cout<<"empty true : " << list3.empty() << endl;
   cout<<"head empty "<<list3.get_first()<<endl;
   cout<<"tail empty "<<list3.get_last()<<endl;

   cout<<"##################insert before head" <<endl;
   list3.insert_before(list3.get_first(),Cat::makeCat());
   cout<<"empty not : "<< list3.empty()<<endl;
   cout<<"head: " << list3.get_first() << endl;
   cout<<"tail: " << list3.get_last() << endl;
   cout<<"size 1: "<< list3.size() <<endl;


   cout<<"##################insert before tail" <<endl;
   list3.insert_before(list3.get_last(),Cat::makeCat());
   cout<<"empty not : "<< list3.empty()<<endl;
   cout<<"head: " << list3.get_first() << endl;
   cout<<"after head "<<list3.get_next(list3.get_first())<<endl;
   cout<<"before tail "<<list3.get_prev(list3.get_last())<<endl;
   cout<<"tail: " << list3.get_last() << endl;
   cout<<"size 2: "<< list3.size() <<endl;
   
   cout<<"##################insert after head" <<endl;
   list3.insert_after(list3.get_first(),Cat::makeCat());
   cout<<"empty not : "<< list3.empty()<<endl;
   cout<<"head: " << list3.get_first() << endl;
   cout<<"after head "<<list3.get_next(list3.get_first())<<endl;
   cout<<"before tail "<<list3.get_prev(list3.get_last())<<endl;
   cout<<"tail: " << list3.get_last() << endl;
   cout<<"size 3: "<< list3.size() <<endl;
  
   cout<<"##################insert after tail" <<endl;
   list3.insert_after(list3.get_last(),Cat::makeCat());
   cout<<"empty not : "<< list3.empty()<<endl;
   cout<<"head: " << list3.get_first() << endl;
   cout<<"after head "<<list3.get_next(list3.get_first())<<endl;
   cout<<"before tail "<<list3.get_prev(list3.get_last())<<endl;
   cout<<"tail: " << list3.get_last() << endl;
   cout<<"size 4: "<< list3.size() <<endl;
  */ 
   

   cout<<"new test for swap on new list"<<endl;
   DoubleLinkedList list2;// = DoubleLinkedList();
   
   cout<<"empty true : " << list2.empty() << endl;
   cout<<"head empty "<<list2.get_first()<<endl;
   cout<<"tail empty "<<list2.get_last()<<endl;
   
   cout<<"################ pushed front "<<endl;
   list2.push_front(Cat::makeCat());
   cout<<"empty false : " << list2.empty() << endl;
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"after head "<<list2.get_next(list2.get_first())<<endl;
   cout<<"before tail "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 1: "<< list2.size()<<endl;
   
   cout<<"################ pushed front "<<endl;
   list2.push_front(Cat::makeCat());
   //list2.push_front(Cat::makeCat());
   cout<<"empty false : " << list2.empty() << endl;
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"after head "<<list2.get_next(list2.get_first())<<endl;
   cout<<"before tail "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 2: "<< list2.size()<<endl;

/*
   cout<<"###########swap head and head "<<endl;
   list2.swap(list2.get_first(),list2.get_first());
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"after head "<<list2.get_next(list2.get_first())<<endl;
   cout<<"before tail "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 1: "<< list2.size()<<endl;

   cout<<"###########swap tail and tail "<<endl;
   list2.swap(list2.get_last(),list2.get_last());
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"after head "<<list2.get_next(list2.get_first())<<endl;
   cout<<"before tail "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 1: "<< list2.size()<<endl;
*/
   cout<<"###########swap head and tail"<<endl;
   list2.swap(list2.get_first(),list2.get_last());
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<"after head "<<list2.get_next(list2.get_first())<<endl;
   cout<<"before tail "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   cout<<"size 1: "<< list2.size()<<endl;
   
   cout<<"##########push two stuff"<<endl;
   list2.push_front(Cat::makeCat());
   list2.push_front(Cat::makeCat());
   
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<" 2nd item "<<list2.get_next(list2.get_first())<<endl;
   cout<<" 3rd item "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;
   list2.swap(list2.get_prev(list2.get_last()),list2.get_next(list2.get_first()));
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<" 2nd item "<<list2.get_next(list2.get_first())<<endl;
   cout<<" 3rd item "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;

   cout<<"adding one more and break it "<<endl;
   list2.push_front(Cat::makeCat());

   cout<<"swap head and tail for more than two"<<endl; 
   list2.swap(list2.get_last(),list2.get_first());
   cout<<"head  "<<list2.get_first()<<endl;
   cout<<" 2nd item "<<list2.get_next(list2.get_first())<<endl;
   cout<<" 3rd item "<<list2.get_prev(list2.get_last())<<endl;
   cout<<"tail  "<<list2.get_last()<<endl;

   cout<<list2.isSorted()<<endl;
   cout<<"end?"<<endl;
}
